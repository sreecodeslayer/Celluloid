// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify, { theme: {
  primary: '#4527A0',
  secondary: '#B39DDB',
  accent: '#FF6D00',
  error: '#f44336',
  warning: '#FFC400',
  info: '#2196f3',
  success: '#8BC34A',
  default: '#7293A0'
}
})

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
