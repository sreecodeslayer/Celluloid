import Vue from 'vue'
import Router from 'vue-router'
import Recents from '@/components/Recents'

import Meta from 'vue-meta'
Vue.use(Router)
Vue.use(Meta)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Recents',
      component: Recents
    }
  ]
})
