from marshmallow import (
    fields,
    validate,
    post_load
)

from movie_store.extensions import ma
from movie_store.models import Movies
from .base import ObjectId
from .user import UserSchema


class MovieSchema(ma.Schema):
    _id = ObjectId(dump_only=True)
    name = ma.String(required=True)
    torrent_hash = ma.String(required=True)
    magnet_url = ma.String(required=True)
    img_url = ma.String(required=True)
    genres = ma.List(ma.String, dump_only=True)
    added_by = ma.Nested(UserSchema, dump_only=True)
    added_on = ma.DateTime(dump_only=True)

    @post_load
    def make_user(self, data):
        return Movies(**data)
