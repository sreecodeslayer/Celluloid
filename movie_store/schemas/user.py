from marshmallow import (
    fields,
    validate,
    post_load
)

from movie_store.extensions import ma
from movie_store.models import Users
from .base import ObjectId


class UserSchema(ma.Schema):
    _id = ObjectId(dump_only=True)
    username = ma.String(required=True)
    email = ma.String(required=True,
                      validate=validate.Email(
                          error='Not a valid email address'))
    passwd_digest = ma.String(load_only=True, required=True)

    @post_load
    def make_user(self, data):
        return Users(**data)

