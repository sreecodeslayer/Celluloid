from flask import Flask

from movie_store import auth, api
from movie_store.extensions import db, jwt


def create_app(config=None, testing=False, cli=False):
    '''Application factory, used to create application
    '''
    app = Flask('movie_store')

    configure_app(app, testing)
    configure_extensions(app, cli)
    register_blueprints(app)

    return app


def configure_app(app, testing=False):
    '''set configuration for application
    '''
    # default configuration
    app.config.from_object('movie_store.config')

    if testing is True:
        # override with testing config
        app.config.from_object('movie_store.configtest')
    else:
        # override with env variable, fail silently if not set
        app.config.from_envvar(
            'MOVIE_STORE_CONFIG', silent=True)


def configure_extensions(app, cli):
    '''configure flask extensions
    '''
    db.init_app(app)
    jwt.init_app(app)


def register_blueprints(app):
    '''register all blueprints for application
    '''
    app.register_blueprint(auth.views.blueprint)
    app.register_blueprint(api.views.blueprint)
