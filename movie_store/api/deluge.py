import requests
import base64
from movie_store.config import (
    DELUGE_URL, DELUGE_PASS,
    DELUGE_USER
)


class Deluge(object):
    cookies = None
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }

    AUTH_LOGIN = dict(id=1, method='auth.login', params=[DELUGE_PASS])
    GET_TORRENTS = 'web.get_torrents'
    ADD_TORRENT = dict(
        id=3,
        method='web.add_torrent'
    )
    REMOVE_TORRENT = dict(
        id=4,
        method='core.remove_torrent'
    )
    UPDATE_UI = dict(
        id=2,
        method='web.update_ui',
        params=[
            [
                'queue', 'name', 'total_wanted', 'state',
                'progress', 'num_seeds', 'total_seeds',
                'num_peers', 'total_peers', 'download_payload_rate',
                'upload_payload_rate', 'eta', 'ratio',
                'distributed_copies', 'is_auto_managed', 'time_added',
                'tracker_host', 'save_path', 'total_done',
                'total_uploaded', 'max_download_speed',
                'max_upload_speed', 'seeds_peers_ratio'
            ], {}
        ]
    )

    def __init__(self):
        self.login()

    def login(self):
        resp = requests.post(
            DELUGE_URL, json=self.AUTH_LOGIN,
            headers=self.headers
        )
        if resp.status_code == 200:
            self.cookies = resp.cookies
            return True
        else:
            return False

    def update_ui(self):
        resp = requests.post(
            DELUGE_URL, json=self.UPDATE_UI,
            headers=self.headers, cookies=self.cookies
        )
        return resp

    def add_torrent(self, magnet):
        self.ADD_TORRENT['params'] = [[{
            'path': magnet, 'options': {
                'file_priorities': [], 'add_paused': False,
                'compact_allocation': False,
                'download_location': '/home/%s/Videos' % (DELUGE_USER),
                'move_completed': False,
                'move_completed_path': '/home/%s/Downloads' % (DELUGE_USER),
                'max_connections': -1,
                'max_download_speed': -1,
                'max_upload_slots': -1,
                'max_upload_speed': -1,
                'prioritize_first_last_pieces': False
            }
        }]]
        resp = requests.post(
            DELUGE_URL, json=self.ADD_TORRENT,
            headers=self.headers, cookies=self.cookies
        )
        return resp

    def delete_torrent(self, torrent_hash, clean_data=False):
        self.REMOVE_TORRENT['params'] = [
            [torrent_hash, clean_data]
        ]
        resp = requests.post(
            DELUGE_URL, json=self.ADD_TORRENT,
            headers=self.headers, cookies=self.cookies
        )
        return resp
