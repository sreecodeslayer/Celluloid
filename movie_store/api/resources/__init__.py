from .user import (
    UserResource,
    UsersResource
)

from .movies import (
	MovieResource,
	MoviesResource
)