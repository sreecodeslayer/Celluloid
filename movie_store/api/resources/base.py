from flask_restful import Resource
from flask_jwt_extended import jwt_required

class JWTResource(Resource):
	decorators = [jwt_required]