from .base import JWTResource
from flask import make_response

from movie_store.schemas import (
    MovieSchema
)
from movie_store.models import (
    Movies
)


class MovieResource(JWTResource):

    def get(self, mid):
        schema = MovieSchema()
        movie = Movies.objects.get_or_404(id=uid)
        return schema.jsonify(movie)

    def patch(self, mid):
        schema = MovieSchema(partial=True)

        if not request.is_json:
            return make_response(
                jsonify({'msg': 'Missing JSON in request'}), 400
            )

        movie = Movies.objects.get_or_404(id=mid)

        movie, errors = schema.load(request.json)
        if errors:
            return errors, 422

        movie.save()
        return schema.jsonify(movie)

    def delete(self, mid):
        movie = Movies.objects.get_or_404(id=mid)
        movie.delete()
        return {'msg': 'Movie deleted'}


class MoviesResource(JWTResource):

    def get(self):
        schema = MovieSchema(many=True)

        movies = Movies.objects()
        return paginate(movies, schema)

    def post(self):
        schema = MovieSchema()

        movie, errors = schema.load(request.json)
        if errors:
            return errors, 422

        movie.save()
        return schema.jsonify(movie)
