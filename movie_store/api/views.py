from flask import Blueprint
from flask_restful import Api

from movie_store.api.resources import (
    UserResource,
    UsersResource
)
from movie_store.api.resources import (
    MovieResource,
    MoviesResource
)


blueprint = Blueprint('api', __name__, url_prefix='/api/v1')
api = Api(blueprint)


api.add_resource(UserResource, '/users/<uid>')
api.add_resource(UsersResource, '/users')


api.add_resource(MovieResource, '/movies/<mid>')
api.add_resource(MoviesResource, '/movies')