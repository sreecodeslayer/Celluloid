from movie_store.extensions import db, pwd_context
from datetime import datetime, timedelta
from .users import Users


class Movies(db.Document):
    '''Mongodb Model for Movies'''
    name = db.StringField(required=True)
    torrent_hash = db.StringField(required=True)
    magnet_url = db.StringField(required=True)
    img_url = db.StringField(required=True)
    genres = db.ListField()
    added_by = db.ReferenceField(Users)
    added_on = db.DateTimeField(default=datetime.utcnow)

    meta = {
        'indexes': ['torrent_hash', 'added_on']
    }

    def __init__(self, **kwargs):
        super(Movies, self).__init__(**kwargs)

    def __repr__(self):
        return '(Movie : %s)' % self.name
