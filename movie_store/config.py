DEBUG = True
SECRET_KEY = 'th1s 1s th3 sup3r s3cr3t k3y for m0vi3 st0r3'

HOST = 'localhost'
PORT = 27017
DB = 'MOVIE_STORE'

MONGODB_SETTINGS = [{
    'db': DB,
    'host': HOST,
    'port': PORT
}]

DELUGE_URL = "http://192.168.10.109:8889/json"
DELUGE_PASS = 'deluge'
DELUGE_USER = 'sreenadh'
